﻿Shader "BallGame/FadeShader" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        _minFade ("minFade", Range (0,1)) = 0.2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {

        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
			"PreviewType"="Plane"
        }

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

        Pass {
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };

            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
				float4 projPos : TEXCOORD1;
            };

            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
				o.projPos = ComputeScreenPos (o.pos);

                return o;
            }

			uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _minFade;

            float4 frag(VertexOutput i) : COLOR {
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float alpha = (_MainTex_var.a*i.vertexColor.a);

                float2 sceneUVs = (i.projPos.xy / i.projPos.w);
				float3 fade = sceneUVs.g + _minFade;

                float3 emissive = (_MainTex_var.rgb*fade*i.vertexColor)*alpha;

                return fixed4(emissive,alpha);
            }

            ENDCG
        }
    }

    FallBack "Sprite-Default"
}
