﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallGame.Screen
{
    /// <summary>
    /// т.к. в задании не было ничего сказано про игровую область, я решил использовать весь экарн.
    /// Класс при создании находит координаты краёв экрана.
    /// </summary>
    public class ScreenBorder
    {
        static ScreenBorder instance;
        public static ScreenBorder Instance
        {
            get
            {
                if (instance == null)
                    instance = new ScreenBorder();

                return instance;
            }
        }

        float top;
        float bottom;
        float left;
        float right;

        private ScreenBorder()
        {
            
            Vector3 topLeftCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.nearClipPlane));
            top = topLeftCorner.y;
            left = topLeftCorner.x;

            Vector3 bottomRightCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, Camera.main.nearClipPlane));
            bottom = bottomRightCorner.y;
            right = bottomRightCorner.x;
        }

        public float GetTop()
        {
            return top;
        }

        public float GetBottom()
        {
            return bottom;
        }

        public float GetLeft()
        {
            return left;
        }

        public float GetRight()
        {
            return right;
        }
    }
}
