﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallGame.UI
{
    /// <summary>
    /// Текст таймера
    /// </summary>
    public class TimerText : MonoBehaviour
    {
        Text timerText;

        private void Awake()
        {
            timerText = GetComponent<Text>();
            timerText.text = "0";

            GameTimer.OnTimerUpdated += UpdateTimer;
            GameTimer.OnTimerOver += Hide;
        }

        void UpdateTimer(int timer)
        {
            //Мне показалось излишним форматировать в минуты и секунды
            timerText.text = timer.ToString();
        }

        void Hide()
        {
            timerText.text = "";
        }

        private void OnDestroy()
        {
            GameTimer.OnTimerUpdated -= UpdateTimer;
            GameTimer.OnTimerOver -= Hide;
        }
    }
}
