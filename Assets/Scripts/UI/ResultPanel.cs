﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallGame.UI
{
    /// <summary>
    /// Панель с результатами
    /// </summary>
    public class ResultPanel : MonoBehaviour
    {
        [SerializeField]
        Text scoreText;

        private void Awake()
        {
            //Чтобы объект своевременно подписался на событие он должен быть активен в начале игры. После он отключается. Иногда это создает проблемы.
            GameTimer.OnTimerOver += Show;
            gameObject.SetActive(false);
        }

        void Show()
        {
            gameObject.SetActive(true);
            scoreText.text = Score.Instance.GetScore().ToString();
        }

        private void OnDestroy()
        {
            GameTimer.OnTimerOver -= Show;
        }
    }
}
