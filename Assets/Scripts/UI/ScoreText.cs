﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallGame.UI
{
    /// <summary>
    /// Текст с колличеством очков
    /// </summary>
    public class ScoreText : MonoBehaviour
    {
        Text scoreText;

        private void Awake()
        {
            scoreText = GetComponent<Text>();
            scoreText.text = "0";

            Score.Instance.OnScoreUpdated += UpdateScore;
            GameTimer.OnTimerOver += Hide;
        }

        void UpdateScore(int score)
        {
            scoreText.text = score.ToString();
        }

        void Hide()
        {
            scoreText.text = "";
        }

        private void OnDestroy()
        {
            Score.Instance.OnScoreUpdated -= UpdateScore;
        }
    }
}
