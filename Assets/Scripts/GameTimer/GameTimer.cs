﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallGame
{
    /// <summary>
    /// Игровой таймер. Я сделал его через корутину для более простого и понятного кода. В игре не нужна реализация паузы, иначе бы этот способ не сработал.
    /// </summary>
    public class GameTimer : MonoBehaviour
    {
        int timer;

        public static event System.Action<int> OnTimerUpdated;
        public static event System.Action OnTimerOver;

        /// <summary>
        /// Ускорение шариков в зависимости от таймера. Вообще не очень правильно размещать это здесь, но лучшего места не нашел
        /// </summary>
        static float acceleration;

        void Start()
        {
            timer = Settings.Instance.timer;

            if (OnTimerUpdated != null)
                OnTimerUpdated(timer);

            StartCoroutine(Tick());
        }

        IEnumerator Tick()
        {
            while(timer > 0)
            {
                yield return new WaitForSeconds(1);
                timer--;

                acceleration = Mathf.Lerp(Settings.Instance.maxTimerAcceleration, Settings.Instance.minTimerAcceleration, (float)timer / (float)Settings.Instance.timer);

                if (OnTimerUpdated != null)
                    OnTimerUpdated(timer);
            }

            if (OnTimerOver != null)
                OnTimerOver();
        }

        public static float GetAcceleration()
        {
            return acceleration;
        }
    }
}
