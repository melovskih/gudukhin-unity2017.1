﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallGame.Screen;

namespace BallGame.Ball
{
    /// <summary>
    /// Я не понял, должн ли быть определенный набор шариков со своим размером, цветом, скоростью и наградой, или шарик может быть любого размера, а остальные параметры рассчитываются исходя из него,
    /// так что взял второй вариант, первый намного проще.
    /// Так же я не стал разделять класс на отдельные компоненты, чтобы не запутывать код.
    /// </summary>
    public class Ball : MonoBehaviour
    {
        /// <summary>
        /// Размер шарика. Равен его scale по x и y, ну лучше хранить это значение отдельно.
        /// </summary>
        float size;

        /// <summary>
        /// Скорость шарика
        /// </summary>
        float speed;

        /// <summary>
        /// Направление движения. Задается один раз при создании.
        /// </summary>
        public Vector3 moveVector;

        /// <summary>
        /// При создании шарик получает размер, все остальные параметры он сам высчитывает исходя из него.
        /// </summary>
        /// <param name="newSize"></param>
        public void Create(float newSize)
        {
            size = newSize;
            transform.localScale = new Vector3(size, size, 1);
            moveVector = new Vector3(transform.position.x, ScreenBorder.Instance.GetTop()+size, 0);
            GetComponent<SpriteRenderer>().color = Settings.Instance.GetRandomColor();
            speed = Settings.Instance.GetBallSpeed(size);
        }

        /// <summary>
        /// Каждый шарик сам отвечает за своё движение. В целях оптимизации можно было бы создать отдельный класс, который двигает все шарики за раз.
        /// Когда шарик выходит за границу экрана, он уничтожается.
        /// </summary>
        void FixedUpdate()
        {
            transform.position = Vector3.MoveTowards(transform.position, moveVector, speed + GameTimer.GetAcceleration());

            if (transform.position.y - (size/2) >= ScreenBorder.Instance.GetTop())
                gameObject.SetActive(false);
        }

        /// <summary>
        /// Для клика по шарику я решил использовать коллайдер и функцию OnMouseDown, чтобы не усложнять код.
        /// Альтернативное решение - в отдельном классе детектировать клик и проходя по всем шарикам проверять был ли клик внутри их радиуса.
        /// </summary>
        private void OnMouseDown()
        {
            Score.Instance.AddScore(Settings.Instance.GetBallScore(size));
            gameObject.SetActive(false);
        }
    }
}
