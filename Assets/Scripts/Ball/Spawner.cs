﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallGame.Screen;

namespace BallGame.Ball
{
    /// <summary>
    /// Класс спавнит шарики раз в несколько секунд
    /// </summary>
    public class Spawner : MonoBehaviour
    {
        /// <summary>
        /// Префаб шарика
        /// </summary>
        [SerializeField]
        GameObject ball;

        bool isSpawn;

        private void Awake()
        {
            GameTimer.OnTimerOver += StopSpawn;
        }

        private void Start()
        {
            StartSpawn();
        }

        /// <summary>
        /// Запускает спавнер
        /// </summary>
        void StartSpawn()
        {
            isSpawn = true;
            StartCoroutine(Spawn());
        }

        /// <summary>
        /// Основная функция спавна
        /// </summary>
        /// <returns></returns>
        IEnumerator Spawn()
        {
            while (isSpawn)
            {
                //Задается случайный размер шарика
                float size = Random.Range(Settings.Instance.minSize, Settings.Instance.maxSize);
                float radius = size / 2f;

                //Создается новый шарик или берется уже уничтоженный. Шарику задается случайная позиция с учетом границ экрана и его размера. Шарик создается за нижней границей экрана.
                GameObject newBall = SpawnBall();
                newBall.transform.position = new Vector3(Random.Range(ScreenBorder.Instance.GetLeft() + radius, ScreenBorder.Instance.GetRight() - radius), ScreenBorder.Instance.GetBottom() - radius, 0);
                newBall.GetComponent<Ball>().Create(size);

                yield return new WaitForSeconds(Settings.Instance.spawnDelay);
            }
        }

        /// <summary>
        /// Создание нового шарика или получение неактивного
        /// </summary>
        /// <returns></returns>
        GameObject SpawnBall()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (!transform.GetChild(i).gameObject.activeSelf)
                {
                    transform.GetChild(i).gameObject.SetActive(true);
                    return transform.GetChild(i).gameObject;
                }
            }

            GameObject newBall = Instantiate(ball);
            newBall.transform.SetParent(transform);
            return newBall;
        }

        /// <summary>
        /// Остановить спавн и спрятать все шарики
        /// </summary>
        void StopSpawn()
        {
            isSpawn = false;
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
