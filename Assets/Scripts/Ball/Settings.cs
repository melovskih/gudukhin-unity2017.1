﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallGame
{
    /// <summary>
    /// Настройки игры
    /// </summary>
    public class Settings : MonoBehaviour
    {
        public static Settings Instance;

        private void Awake()
        {
            Instance = this;
        }

        public float minSize;
        public float maxSize;

        public float minSpeed;
        public float maxSpeed;

        public float minScore;
        public float maxScore;

        public float minTimerAcceleration;
        public float maxTimerAcceleration;

        public float spawnDelay;

        public int timer;

        public Color[] availableColors;

        /// <summary>
        /// Задает скорость шарика относительно его размера
        /// Мне показалось правильным решением разместить эту функцию в настройках
        /// </summary>
        public float GetBallSpeed(float ballSize)
        {
            return Mathf.Lerp(maxSpeed, minSpeed, ((ballSize - minSize) / (maxSize - minSize)));
        }

        /// <summary>
        /// Вычисляет кол-во очков в зависимости от размера шарика
        /// </summary>
        public int GetBallScore(float ballSize)
        {
            return Mathf.RoundToInt(Mathf.Lerp(maxScore, minScore, ((ballSize - minSize) / (maxSize - minSize))));
        }

        public Color GetRandomColor()
        {
            return availableColors[Random.Range(0, availableColors.Length)];
        }
    }
}
