﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallGame
{
    /// <summary>
    /// Класс очков
    /// </summary>
    public class Score
    {
        static Score instance;
        public static Score Instance
        {
            get
            {
                if (instance == null)
                    instance = new Score();

                return instance;
            }
        }

        int score;

        /// <summary>
        /// При обновлении очков посылает событие с их колличеством
        /// </summary>
        public event System.Action<int> OnScoreUpdated;

        private Score()
        {
            Clear();
        }

        public int GetScore()
        {
            return score;
        }

        public void AddScore(int newScore)
        {
            score += newScore;

            if (OnScoreUpdated != null)
                OnScoreUpdated(score);
        }

        public void Clear()
        {
            score = 0;

            if (OnScoreUpdated != null)
                OnScoreUpdated(score);
        }
    }
}
